import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter

public class Vacancy {
    private String nameCompany;
    private String link;
    private String nameVacancy;

    public Vacancy(String nameCompany, String link, String nameVacancy) {
        this.nameCompany = nameCompany;
        this.link = link;
        this.nameVacancy = nameVacancy;
    }

    @Override
    public String toString() {
        return "Vacancy{" +
                "nameCompany='" + nameCompany + '\'' +
                ", link='" + link + '\'' +
                ", nameVacancy='" + nameVacancy + '\'' +
                '}';
    }
}
