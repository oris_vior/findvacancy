import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BrowserDriver {
    public static final String START_PAGE_HTML = "src/main/resources/StartPage.html";
    private final WebDriver driver = new ChromeDriver();
    private final JavascriptExecutor executor = (JavascriptExecutor) driver;
    private Map<String, String> company;
    private List<Vacancy> vacancy;

    public void loadStartWebPage(int count) {
        driver.navigate().to("https://jobs.dou.ua/companies/");

        while (count != 0) {
            driver.findElement(By.linkText("Більше компаній")).click();
            count--;
        }

        company = getStartPage();

        vacancy = getActualVacancy(company);

        saveVacancyToFile(vacancy);

        driver.quit();
    }

    @SneakyThrows
    private void saveVacancyToFile(List<Vacancy> vacancy) {
        File file = new File("src/main/resources/vacancy.json");
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(file, vacancy);

    }


    @SneakyThrows
    private List<Vacancy> getActualVacancy(Map<String, String> company) {
        File file = new File("src/main/resources/vacancy.html");
        List<Vacancy> vacancyList = new ArrayList<>();


        for (String link : company.values()) {
            driver.navigate().to(link);

            while (driver.findElements(By.partialLinkText("Більше вакансій")).size() > 0) {
                checkElementOnPage(driver);
            }
            executor.executeScript("arguments[0].scrollIntoView();", driver.findElement(By.partialLinkText("Пошук програмістів на Джині")));
            vacancyList.addAll(getVacancy(driver.getPageSource()));
        }
        return vacancyList;
    }


    private void checkElementOnPage(WebDriver driver) {
        try {
            WebElement button = driver.findElement(By.partialLinkText("Більше вакансій"));
            executor.executeScript("arguments[0].scrollIntoView(true);", button);
            button.click();

        } catch (Exception e) {
            System.out.println(e.getMessage());

        }

    }


    private List<Vacancy> getVacancy(String pageSource) {
        List<Vacancy> vacancyList = new ArrayList<>();
        Document document = Jsoup.parse(pageSource, "http://example.com/");
        Elements elements = document.getElementsByClass("vt");
        for (Element element : elements) {
            Vacancy vacancy = new Vacancy(document.getElementsByClass("g-h2").text(), element.attr("href"), element.text());
            vacancyList.add(vacancy);
        }
        return vacancyList;
    }

    @SneakyThrows
    private Map<String, String> getStartPage() {
        File file = getFileStartPage();
        return parseStartPage(file);
    }

    private Map<String, String> parseStartPage(File file) throws IOException {
        Map<String, String> company = new LinkedHashMap<>();
        Document document = Jsoup.parse(file, "UTF-8", "http://example.com/");
        Elements el = document.getElementsByClass("cn-a");
        for (Element element : el) {
            company.put(element.text(), element.attr("href") + "vacancies/");
        }
        return company;
    }

    private File getFileStartPage() {
        File file = new File(START_PAGE_HTML);
        try (FileWriter writer = new FileWriter(file, false)) {
            writer.write(driver.getPageSource());
            return file;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
